package com.tutos.elvis.androidvolley;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by elvis on 01/10/2016.
 */
public class PresentacionLayout extends AppCompatActivity {

    private TextView tvSaludo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.presentacion_layout);

        tvSaludo = (TextView) findViewById(R.id.tvSaludo);
        llenarSaludo();
    }

    private void llenarSaludo() {
        Bundle bundle = getIntent().getExtras();
        tvSaludo.setText("Nombre: "+bundle.getString("nombre")+"\nDNI: "+bundle.getString("dni")+"\nSexo: "+bundle.getString("sexo"));
    }
}
