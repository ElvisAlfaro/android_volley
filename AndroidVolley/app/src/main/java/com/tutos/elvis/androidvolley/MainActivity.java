package com.tutos.elvis.androidvolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.tutos.elvis.androidvolley.VolleyController.AppController;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText etUsuario;
    private EditText etPass;
    private String urlLogin = "http://192.168.1.36/login/procesar_login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsuario = (EditText) findViewById(R.id.etUser);
        etPass = (EditText) findViewById(R.id.etPass);
    }

    public void btnAceptar(View view){
        final String usuario = etUsuario.getText().toString();
        final String password = etPass.getText().toString();

        // INICIAR LA CONEXION CON VOLLEY
        StringRequest request = new StringRequest(Request.Method.POST, urlLogin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // SE EJECUTA CUANDO LA CONSULTA SALIO BIEN, SIN ERRORES
                        if(response.equals("0")) {
                            Toast.makeText(MainActivity.this, "Datos de usuario incorrecto...", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                System.out.println("RESPUESTA DE SERVIDOR : "+response);
                                Toast.makeText(MainActivity.this, response, Toast.LENGTH_LONG).show();
                                JSONArray jsonArray = new JSONArray(response);
                                // OBTENEMOS LOS DATOS QUE DEVUELVE EL SERVIDOR
                                String nombre = jsonArray.getJSONObject(0).getString("nombre");
                                String DNI = jsonArray.getJSONObject(0).getString("dni");
                                String sexo = jsonArray.getJSONObject(0).getString("sexo");

                                // ABRIMOS UNA NUEVA ACTIVITY Y MANDAMOS LOS DATOS QUE SE VAN A MOSTRAR
                                Intent intent = new Intent(MainActivity.this, PresentacionLayout.class);
                                intent.putExtra("nombre", nombre);
                                intent.putExtra("dni", DNI);
                                intent.putExtra("sexo", sexo);
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // SE EJECUTA CUANDO ALGO SALIO MAL AL INTENTAR HACER LA CONEXION
                        Toast.makeText(MainActivity.this, "ERROR DE CONEXION...", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // AQUI SE ENVIARAN LOS DATOS EMPAQUETADOS EN UN OBJETO MAP<clave, valor>
                Map<String, String> parametros = new HashMap<>();
                parametros.put("usuario", usuario);
                parametros.put("pass", password);
                return parametros;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }
}
